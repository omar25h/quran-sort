package main

import (
	"bufio"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"strconv"
	"strings"

	id3 "github.com/mikkyang/id3-go"
	"github.com/mikkyang/id3-go/v2"
)

var titles = map[string]string{
	"001": "001 - سورة الفاتحة",
	"002": "002 - سورة البقرة",
	"003": "003 - سورة آل عمران",
	"004": "004 - سورة النساء",
	"005": "005 - سورة المائدة",
	"006": "006 - سورة الأنعام",
	"007": "007 - سورة الأعراف",
	"008": "008 - سورة الأنفال",
	"009": "009 - سورة التوبة",
	"010": "010 - سورة يونس",
	"011": "011 - سورة هود",
	"012": "012 - سورة يوسف",
	"013": "013 - سورة الرعد",
	"014": "014 - سورة إبراهيم",
	"015": "015 - سورة الحجر",
	"016": "016 - سورة النحل",
	"017": "017 - سورة الإسراء",
	"018": "018 - سورة الكهف",
	"019": "019 - سورة مريم",
	"020": "020 - سورة طه",
	"021": "021 - سورة الأنبياء",
	"022": "022 - سورة الحج",
	"023": "023 - سورة المؤمنون",
	"024": "024 - سورة النّور",
	"025": "025 - سورة الفرقان",
	"026": "026 - سورة الشعراء",
	"027": "027 - سورة النمل",
	"028": "028 - سورة القصص",
	"029": "029 - سورة العنكبوت",
	"030": "030 - سورة الروم",
	"031": "031 - سورة لقمان",
	"032": "032 - سورة السجدة",
	"033": "033 - سورة الأحزاب",
	"034": "034 - سورة سبأ",
	"035": "035 - سورة فاطر",
	"036": "036 - سورة يس",
	"037": "037 - سورة الصّافّات",
	"038": "038 - سورة ص",
	"039": "039 - سورة الزمر",
	"040": "040 - سورة غافر",
	"041": "041 - سورة فصّلت",
	"042": "042 - سورة الشورى",
	"043": "043 - سورة الزخرف",
	"044": "044 - سورة الدخان",
	"045": "045 - سورة الجاثية",
	"046": "046 - سورة الأحقاف",
	"047": "047 - سورة محمّد",
	"048": "048 - سورة الفتح",
	"049": "049 - سورة الحُجُرات",
	"050": "050 - سورة ق",
	"051": "051 - سورة الذاريات",
	"052": "052 - سورة الطور",
	"053": "053 - سورة النجم",
	"054": "054 - سورة القمر",
	"055": "055 - سورة الرحمن",
	"056": "056 - سورة الواقعة",
	"057": "057 - سورة الحديد",
	"058": "058 - سورة المجادلة",
	"059": "059 - سورة الحشر",
	"060": "060 - سورة الممتحنة",
	"061": "061 - سورة الصّف",
	"062": "062 - سورة الجمعة",
	"063": "063 - سورة المنافقون",
	"064": "064 - سورة التغابن",
	"065": "065 - سورة الطلاق",
	"066": "066 - سورة التحريم",
	"067": "067 - سورة الملك",
	"068": "068 - سورة القلم",
	"069": "069 - سورة الحاقّة",
	"070": "070 - سورة المعارج",
	"071": "071 - سورة نوح",
	"072": "072 - سورة الجن",
	"073": "073 - سورة المزّمّل",
	"074": "074 - سورة المّدّثّر",
	"075": "075 - سورة القيامة",
	"076": "076 - سورة الإنسان",
	"077": "077 - سورة المرسلات",
	"078": "078 - سورة النبأ",
	"079": "079 - سورة النازعات",
	"080": "080 - سورة عبس",
	"081": "081 - سورة التكوير",
	"082": "082 - سورة الانفطار",
	"083": "083 - سورة المطففين",
	"084": "084 - سورة الانشقاق",
	"085": "085 - سورة البروج",
	"086": "086 - سورة الطارق",
	"087": "087 - سورة الأعلى",
	"088": "088 - سورة الغاشية",
	"089": "089 - سورة الفجر",
	"090": "090 - سورة البلد",
	"091": "091 - سورة الشمس",
	"092": "092 - سورة الليل",
	"093": "093 - سورة الضحى",
	"094": "094 - سورة الشرح",
	"095": "095 - سورة التين",
	"096": "096 - سورة العلق",
	"097": "097 - سورة القدر",
	"098": "098 - سورة البيّنة",
	"099": "099 - سورة الزلزلة",
	"100": "100 - سورة العاديات",
	"101": "101 - سورة القارعة",
	"102": "102 - سورة التكاثر",
	"103": "103 - سورة العصر",
	"104": "104 - سورة الهمزة",
	"105": "105 - سورة الفيل",
	"106": "106 - سورة قريش",
	"107": "107 - سورة الماعون",
	"108": "108 - سورة الكوثر",
	"109": "109 - سورة الكافرون",
	"110": "110 - سورة النصر",
	"111": "111 - سورة المسد",
	"112": "112 - سورة الإخلاص",
	"113": "113 - سورة الفلق",
	"114": "114 - سورة الناس",
}

func main() {
	fmt.Print("Enter Directory Name: ")
	dirName := ""
	scanner := bufio.NewScanner(os.Stdin)
	if scanner.Scan() {
		dirName = scanner.Text()
	}
	fmt.Print("Enter Sheikh Name: ")
	sheikh := ""
	if scanner.Scan() {
		sheikh = scanner.Text()
	}
	if sheikh == "" {
		log.Fatal("sheikh name cannot be empty")
	}

	year := ""
	if scanner.Scan() {
		year = scanner.Text()
	}

	log.Println("dirName: ", dirName)
	log.Println("sheikh: ", sheikh)
	log.Println("year: ", year)

	files, err := ioutil.ReadDir(dirName)
	if err != nil {
		log.Fatalf("could not read directory %s: %v\n", dirName, err)
	}
	for _, v := range files {
		fullPath := fmt.Sprintf("%s/%v", dirName, v.Name())
		ext := filepath.Ext(v.Name())
		baseName := strings.TrimSuffix(v.Name(), ext)
		// num := fmt.Sprintf("%03s", v.Name())
		log.Println("filePath ", fullPath, " baseName", baseName, " ext", ext)

		if ext == ".mp3" {
			if _, ok := titles[baseName]; !ok {
				log.Printf("file %s\n is not registered", baseName)
				continue
			}
			dest := filepath.Join(dirName, titles[baseName]+ext)
			fmt.Printf("Source: %s / Destination: %s\n", fullPath, dest)
			err := os.Rename(fullPath, dest)
			if err != nil {
				log.Printf("could not rename file: %v\n", err)
			}

			file, err := id3.Open(dest)
			if err != nil {
				log.Printf("could not open %s: %v\n", dest, err)
				continue
			}
			defer file.Close()
			file.SetGenre("قرآن")
			file.SetTitle(titles[baseName])
			file.SetArtist(sheikh)
			file.SetAlbum("مصحف " + sheikh)
			if year != "" {
				file.SetYear(year)
			}
			i, err := strconv.Atoi(baseName)
			if err != nil {
				log.Printf("could not parse int in %s\n", baseName)
				continue
			}

			ft := v2.V23FrameTypeMap["TRCK"]
			log.Println("track frame: ", fmt.Sprintf("%d/114", i))
			trackFrame := v2.NewTextFrame(ft, fmt.Sprintf("%d/114", i))
			file.AddFrames(trackFrame)

		}
	}
}
